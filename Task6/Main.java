import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

public class Main {

    public static void main(String[] args) throws InterruptedException {

        int[] numbers = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

        LinkedBlockingQueue<Runnable> queue = new LinkedBlockingQueue<>();

        queue.put(() -> {
            
            int min = numbers[0];
            int max = numbers[0];
            for (int i = 1; i < numbers.length; i++) {
                if (numbers[i] < min) {
                    min = numbers[i];
                }
                if (numbers[i] > max) {
                    max = numbers[i];
                }
            }
            System.out.println("Min: " + min + ", Max: " + max);
        });

        queue.put(() -> {
            
            double sum = 0;
            for (int i = 0; i < numbers.length; i++) {
                sum += numbers[i];
            }
            double average = sum / numbers.length;
            System.out.println("Average: " + average);
        });

        queue.put(() -> {
            
            List<Integer> filtered = new ArrayList<>();
            for (int i = 0; i < numbers.length; i++) {
                if (numbers[i] % 2 == 0) {
                    filtered.add(numbers[i]);
                }
            }
            System.out.println("Filtered: " + filtered);
        });

        
        int numThreads = Runtime.getRuntime().availableProcessors();
        WorkerThread[] threads = new WorkerThread[numThreads];
        for (int i = 0; i < numThreads; i++) {
            threads[i] = new WorkerThread(queue);
            threads[i].start();
        }

        
        for (int i = 0; i < numThreads; i++) {
            threads[i].join();
        }
    }

}
